angular.module('userFormDirective', []).directive('userForm', function () {
    return {
        restrict: 'E',
        templateUrl: 'views/partials/user-form.html',
        controller: function ($scope) {
            var user = JSON.parse(localStorage.getItem('user'));

            $scope.user = user ? user : {};

            $scope.submit = function () {
                //Save user to localStorage
                localStorage.setItem('user', JSON.stringify($scope.user));
                //Reload page to demonstrate result
                location.reload();
            }
        }
    };
});