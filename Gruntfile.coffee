module.exports = (grunt) ->
  require('load-grunt-tasks')(grunt)
  require('time-grunt')(grunt)

  appConfig =
    app: require('./bower.json').appPath || 'app',
    dist: 'dist'

  # Project configuration.
  grunt.initConfig

    config: appConfig

    bower_concat:
      dist:
        dest: '<%= config.app %>/js/vendor.js'

    uglify:
      dist:
        options:
          mangle: false,
          compress: true
        files: [
          '<%= config.dist %>/js/vendor.min.js': '<%= config.app %>/js/vendor.js'
        ]

    cssmin:
      dist:
        files: [
          expand: true,
          cwd: 'tmp',
          src: ['*.css'],
          dest: '<%= config.dist %>/css',
          ext: '.min.css'
        ]

    clean:
      dist:
        src: 'tmp'

    copy:
      tmp:
        files: [
          expand: true
          cwd: 'tmp'
          src: 'app.min.js'
          dest: '<%= config.dist %>/js'
        ]
      views:
        files: [
          {
            expand: true
            cwd: '<%= config.app %>'
            src: ['views/**/*.html']
            dest: '<%= config.dist %>'
          }
        ]
      dist:
        files: [
          {
            expand: true
            dot: true
            cwd: '<%= config.app %>'
            dest: '<%= config.dist %>'
            src: [
              '*.html',
              'views/**/*.html',
              'config.xml',
              'css/bootstrap.min.css'
              'audio/*'
            ]
          }
        ]